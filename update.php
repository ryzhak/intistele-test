<?php

require_once "QuoteParser.php";

$dbConn = new MySQLDbConnection('localhost', 'username', 'password', 'dbname');
$QP = new QuoteParser($dbConn);
$QP->run($argv);
unset($dbConn);
unset($QP);