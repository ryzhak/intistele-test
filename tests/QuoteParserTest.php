<?php

require 'QuoteParser.php';

class QuoteParserTests extends PHPUnit_Framework_TestCase
{
    private $_QP;

    protected function setUp()
    {
        $dbConn = new MySQLDbConnection('localhost', 'username', 'password', 'dbname');
        $this->_QP = new QuoteParser($dbConn);
        $this->_QP->conn->truncate('currency');
    }

    protected function tearDown()
    {
        $this->_QP = NULL;
    }

    public function testInvalidNumberOfArgs()
    {
        $this->expectOutputRegex('/Error: invalid number of arguments/');
        $this->_QP->run(['update.php', 1, 1]);
    }

    public function testInvalidQuoteSource(){
        $this->expectOutputRegex('/Error: invalid quote source/');
        $this->_QP->run(['update.php', 3]);
    }


    public function testDataInsertFromUrlSrc(){
        $this->_QP->run(['update.php', 1]);
        $this->assertEquals(3, $this->_QP->conn->getRowsCount('currency'));
    }

    public function testDataInsertFromFileSrc(){
        $this->_QP->run(['update.php', 2]);
        $this->assertEquals(3, $this->_QP->conn->getRowsCount('currency'));
    }

    public function testDataUpdate(){
        $this->_QP->run(['update.php', 1]);
        $this->_QP->dataSource = new UrlDataSource('tests/testrates.json');
        $this->_QP->run(['update.php', 1]);
        $row = $this->_QP->conn->selectRowBySymbol('USD');
        $this->assertEquals(1.01, $row['rate']);
    }

    public function testMySQLSelectRowBySymbol(){
        $this->_QP->run(['update.php', 1]);
        $row = $this->_QP->conn->selectRowBySymbol('USD');
        $this->assertEquals(1, $row['rate']);
    }

    public function testMySQLUpdateRateBySymbol(){
        $this->_QP->run(['update.php', 1]);
        $this->_QP->conn->updateRateBySymbol(1.02, 'USD');
        $row = $this->_QP->conn->selectRowBySymbol('USD');
        $this->assertEquals(1.02, $row['rate']);
    }

    public function testMySQLInsertCurrency(){
        $this->_QP->run(['update.php', 1]);
        $this->_QP->conn->insertCurrency(30, 'CHR');
        $row = $this->_QP->conn->selectRowBySymbol('CHR');
        $this->assertEquals(30, $row['rate']);
    }

    public function testMySQLGetRowsCount(){
        $this->_QP->run(['update.php', 1]);
        $count = $this->_QP->conn->getRowsCount('currency');
        $this->assertEquals(3, $count);
    }

}