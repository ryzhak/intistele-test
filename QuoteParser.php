<?php

interface IDataSource {

    public function load();

}

class UrlDataSource implements IDataSource {

    /*
     * @const Argument in the console for this data source
     */
    const SOURCE_ID = 1;

    /*
     * @var string currencies URL
     */
    private $_src;

    /*
     * Sets the initial web source
     *
     * @param string $src Web data source
     */
    function __construct($src = 'http://localhost:8080/rates1.json'){

        $this->_src = $src;

    }

    /*
     * Returns an array of quotes
     *
     * @return array
     */
    function load(){

        $json = file_get_contents($this->_src);
        $quotesFromUrl = json_decode($json);
        //convert URL json to the file json type
        return array_map([$this, 'convertQuotesArray'], $quotesFromUrl->rates);

    }

    /*
     * Maps array from URL type to file type
     *
     * @param object $v Single object from parsed json file
     *
     * @return object
     */
    function convertQuotesArray($v){
        return [$v->symbol => $v->rate];
    }

}

class FileDataSource implements IDataSource {

    /*
     * @const Argument in the console for this data source
     */
    const SOURCE_ID = 2;

    /*
     * @var string path to the file with currencies
     */
    private $_src;

    /*
     * Sets the initial web source
     *
     * @param string $src Web data source
     */
    function __construct($src = 'rates.json'){

        $this->_src = $src;

    }

    /*
     * Returns an array of quotes
     *
     * @return array
     */
    function load(){

        $json = file_get_contents($this->_src);
        return json_decode($json);

    }

}

interface IValidator {

    function validateArgs($args);
    function sanitize($value);

}

class Validation implements IValidator {

    /*
     * Validates command line arguments
     *
     * @param array @args arguments from command line
     *
     * @return boolean
     *
     */
    function validateArgs($args){

        if(count($args) != 2){
            echo "Error: invalid number of arguments\n";
            return false;
        }

        if($args[1] == UrlDataSource::SOURCE_ID || $args[1] == FileDataSource::SOURCE_ID){
            return true;
        } else {
            echo "Error: invalid quote source\n";
            return false;
        }

    }

    /*
     * Sanitizes incoming value
     *
     * @param mixed $value Value to be sanitized from malicious code
     *
     * @return mixed
     */
    function sanitize($value){
        return addslashes(htmlspecialchars($value));
    }

}

interface IDbConnection {

    /*
     * Returns row by symbol name
     *
     * @var string $symbol Symbol
     *
     * @return array
     */
    function selectRowBySymbol($symbol);

    /*
     * Updates rate by symbol
     *
     * @var float $rate Rate
     * @var string $symbol Symbol
     *
     */
    function updateRateBySymbol($rate, $symbol);

    /*
     * Inserts symbol and its rate
     *
     * @var float $rate Rate
     * @var string $symbol Symbol
     *
     */
    function insertCurrency($rate, $symbol);

    /*
     * Truncates table
     *
     * @var string $tableName Table name which we want to clear
     */
    function truncate($tableName);

    /*
     * Returns rows count by table
     *
     * @var string $tableName
     *
     * @return int
     */
    function getRowsCount($tableName);

}

class MySQLDbConnection implements IDbConnection {

    /*
     * @var object DB connection object
     */
    private $_conn;

    /*
     * Creates a database connection
     *
     * @param string $servername Name of the server
     * @param string $username Database username
     * @param string $password Database password
     * @param string $dbname Database name
     *
     * @return void
     */
    function __construct($servername = 'localhost', $username = 'root', $password = '', $dbname = 'default'){
        //create connection
        $this->_conn = new mysqli($servername, $username, $password, $dbname);
        //check connection
        if ($this->_conn->connect_error) {
            die("Connection failed: " . $this->_conn->connect_error);
        }
    }

    function selectRowBySymbol($symbol){
        $sql = "SELECT * FROM currency WHERE symbol='$symbol'";
        return $this->_conn->query($sql)->fetch_assoc();
    }


    function updateRateBySymbol($rate, $symbol){
        $sql = "UPDATE currency SET rate = $rate WHERE symbol='$symbol'";
        if (!$this->_conn->query($sql)){
            echo "Error: " . $sql . "<br>" . $this->_conn->error;
        }
    }

    function insertCurrency($rate, $symbol){
        $sql = "INSERT INTO currency(symbol, rate) VALUES('$symbol', $rate)";
        if (!$this->_conn->query($sql)){
            echo "Error: " . $sql . "<br>" . $this->_conn->error;
        }
    }

    function getRowsCount($tableName){
        $sql = "SELECT * FROM $tableName";
        $result = $this->_conn->query($sql);
        if (!$result){
            echo "Error: " . $sql . "<br>" . $this->_conn->error;
        }
        return $result->num_rows;
    }

    function truncate($tableName){
        $sql = "TRUNCATE $tableName";
        if (!$this->_conn->query($sql)){
            echo "Error: " . $sql . "<br>" . $this->_conn->error;
        }
    }

    /*
     * On object destruction
     */
    function __destruct(){
        $this->_conn->close();
    }

}

class QuoteParser {

    /*
     * @var object DB connection object
     */
    public $conn;

    /*
     * @var object Validator object
     */
    protected $validator;

    /*
     * @var object Data source object
     */
    public $dataSource;

    function __construct(IDbConnection $dbConnection){

        $this->validator = new Validation();
        $this->conn = $dbConnection;

    }

    /*
     * Runs our parser
     *
     * @args array Arguments for out script
     *
     * @return void
     */
    function run($args){

        //validating input
        if(!$this->validator->validateArgs($args)) return;

        if($args[1] == UrlDataSource::SOURCE_ID){
            if(empty($this->dataSource)) $this->dataSource = new UrlDataSource();
        }

        if($args[1] == FileDataSource::SOURCE_ID){
            if(empty($this->dataSource)) $this->dataSource = new FileDataSource();
        }

        $quotes = $this->dataSource->load();

        foreach($quotes as $quote){

            $quote = (array)$quote;

            $symbol = $this->validator->sanitize(key($quote));
            $value = $this->validator->sanitize($quote[$symbol]);

            $row = $this->conn->selectRowBySymbol($symbol);

            //if we already have this symbol in the DB
            if(!empty($row)){

                //if rate was not updated then continue
                if($row['rate'] == $value) continue;

                //update the rate
                $this->conn->updateRateBySymbol($value, $symbol);
            } else {
                //insert a new symbol
                $this->conn->insertCurrency($value, $symbol);
            }

        }

    }

}