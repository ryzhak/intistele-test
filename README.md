# intistele test task #

### How to use? ###

For parsing data from URL run the following command in a terminal:

```
#!php

php index.php 1

```

For parsing data from local file run the following command in a terminal:

```
#!php

php index.php 2

```

### How to run tests? ###

```
#!php

phpunit tests/QuoteParserTest.php

```