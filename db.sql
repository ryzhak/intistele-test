CREATE TABLE `currency` (
	`symbol` VarChar( 4 ) NOT NULL,
	`rate` Decimal( 10, 2 ) NOT NULL )
ENGINE = InnoDB;